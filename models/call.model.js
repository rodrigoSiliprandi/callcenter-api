const mongoose = require ('mongoose')

const callSchema = mongoose.Schema({
    user: mongoose.Types.ObjectId,    
    date: Date,
    descricao: String,
    status: String

})

module.exports = mongoose.model('Call', callSchema)