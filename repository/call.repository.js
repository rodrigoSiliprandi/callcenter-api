const Call = require ('../model/call.model')
const database = require ('../db/database')

exports.findAll = async () => {
    let con = await database.connect()
    let result = await Call.find()

    con.disconnect()
    return result
}

exports.save = async (data) => {
    let con = await database.connect()
    let call = new Call(data)
    await call.save()

    con.disconnect()

}