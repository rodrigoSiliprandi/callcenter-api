const callRoutes = require('./call.routes')
const userRoutes = require('./user.routes')

module.exports = (app) => {
    callRoutes(app)
    userRoutes(app)
}